# Template

## Title: Migration to independent platform for provision of source code

### A. Problem / Goal 

Since the purchase of GitHub by Microsoft in 2018, a dependence on the BigTech corporation can no longer be denied. 

On the one hand, I can understand why GitHub was chosen as the platform for making source code available: "Everyone is here".

On the other hand, I see the danger of a vendor-lockin effect and that open source projects become centrally dependent on Microsoft. In my eyes, this is very dangerous for free and open source software and hardware projects.

In the medium and long term, the goal would be to become independent of GitHub and thus of Microsoft. The Gitea-based Codeberg project of Codeberg e. V. in Berlin would be a good choice here.

There are also (legal) problems with compliance with the licence of GitHub functions, such as the co-pilot. 

###  B. Solution 

My considered solution to the problem described in A. would be the following:

1. A user of this open source project creates a user account on https://codeberg.org/
2. If necessary: This user creates an organisation for the project.
3. A "personal access token" is created on the GitHub account, which has appropriate rights to the organisation repositories, using the developer options in the settings.
4. all repositories would be migrated with this access token into the ownership of the organisation created in step two.

Regarding step four, there is an entry in the documentation of Codeberg: https://docs.codeberg.org/advanced/migrating-repos/ 

### C. Alternatives

A possible alternative would be to perform the first three steps as described in B. A possible alternative would be to perform the first three steps as described in B., and modify the fourth step to include a mirror of GitHub. So that all issues and such that would be created in the GitHub repository would be transferred to the Codeberg repository.

### D. Responsibilities

I would see the responsibility in the owners of the repository and, if necessary, additional project participants.

###  E. Other

Basically, a look at the documentation of Codeberg is not unwise: https://docs.codeberg.org 

Should it be necessary to manage repositories in organisations, this is also possible under Codeberg, see: https://docs.codeberg.org/collaborating/create-organization/ 

Regarding licensing there is a page in the documentation of Codeberg: 
https://docs.codeberg.org/getting-started/licensing/ 

### F. Risk

Last but not least, it must be assumed that people could potentially create fewer issues because it is a new platform and it is less known. It remains to be seen how and when the principle of decentralisation or federation will be implemented in Gitea, on which Codeberg, the GitHub alternative, is based, see the following article: https://social.exozy.me/@ta180m/108631221939677386